# Compass lerna POC

This repo has a script `run.js` that turn compass into a lerna monorepo.

The reason for using a script as POC rather than just a pre-setup repo, is so we are able to experiment with different setup from scratch and re-import changes that are happening in the meanwhile inside compass and compass plugins.

## Usage

``` sh
rm -Rf compass && node run.js

cd compass

npm run install # this should bootstrap + compile
npm run start
```

Setting `export IMPORT_STRATEGY=lerna` will cause a detailed import of commits with authors and dates to take place. By default the cloned repos are only copied inside the `packages` folder.

## TODO

- Deps:
  - DONE -Align deps-
  - DONE -Align devDeps-
  - Move debug and other prod deps to `dependencies`!
  - Align peer deps

- Webpack:
  - DONE -Unify webpack config for plugins-
  - Local dev setup + hot-reload

- Tests:
  - Remove rebuilds (this will solve lots of headaches with deps breaking between tests and start)
    - Use new keytar and remove: node-gyp rebuild --directory ../storage-mixin/node_modules/keytar/
    - replace `kerberos` with `compass-kerberos` (Copy `templates/compass-kerberos` in `packages` if is not there already) and remove the electron rebuild / node-gyp rebuild.
  - Setup tests for plugins (new mocha + karma setup, enzyme ..)
  - Setup tests for normal packages (new mocha ..)

- Checks:
  - ESLINT with unpublished checks (required with hoisted deps) and mocha exclusive
  - depcheck on pre-push only for changed packages (mongodb-js-precommit should still work)
  - depcheck everything on ci (mongodb-js-precommit should still work)

- CI:
  - test setup for PRs that tests all packages
  - point evergreen to the right folder, or adjust scripts to build `packages/compass`

- Misc:
  - Copy and align licenses
  - add Github templates on the top repo

## Notes

#### Open questions

- How to make sure data explorer can still use plugins?
- Is `hadron-build` able to build a release with this setup? What needs to be changed?
- Is `lerna --hoist` necessary or manually hoisting dev deps is enough?

#### How to publish changes to Compass packages:

The repo is setup with:

``` json
  "version": "independent"
```

this way after changes are done by issuing:

``` sh
lerna publish
```

lerna will prompt us to provide a new version for each of the changed
packages and update all the dependencies accordingly.

#### Evergreen

Evergreen would work in the exact same way, however we would have to
update the tasks to point to the right `packages/mongodb-compass` folder

#### Caveats

- `compass-loading` has a dependency on a private repo: "@fortawesome/fontawesome-pro": "^5.10.2"
- `hadron-react-*` is already in a monorepo, has to be imported manually
