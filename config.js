const semver = require('semver')
const generateDependencyReport = require('./scripts/generate-dependency-report')

const {
  devDependencies: reportDevDependencies,
  dependencies: reportDependencies
} = generateDependencyReport();

module.exports = {
  skipPull: true,
  packages: {
    "compass": "https://github.com/mongodb-js/compass",
    // "@mongodb-js/compass-instance-header": "git@github.com:mongodb-js/compass-instance-header.git",
    "@mongodb-js/compass-aggregations": "https://github.com/mongodb-js/compass-aggregations",
    // "@mongodb-js/compass-app-stores": "https://github.com/mongodb-js/compass-app-stores",
    // "@mongodb-js/compass-auth-kerberos": "https://github.com/mongodb-js/compass-auth-kerberos",
    // "@mongodb-js/compass-auth-ldap": "https://github.com/mongodb-js/compass-auth-ldap",
    // "@mongodb-js/compass-auth-x509": "https://github.com/mongodb-js/compass-auth-x509",
    // "@mongodb-js/compass-auto-updates": "https://github.com/mongodb-js/compass-auto-updates",
    // "@mongodb-js/compass-collection": "https://github.com/mongodb-js/compass-collection",
    // "@mongodb-js/compass-collection-stats": "https://github.com/mongodb-js/compass-collection-stats",
    // "@mongodb-js/compass-collections-ddl": "https://github.com/mongodb-js/compass-collections-ddl",
    "@mongodb-js/compass-connect": "https://github.com/mongodb-js/compass-connect",
    // "@mongodb-js/compass-crud": "https://github.com/mongodb-js/compass-crud",
    // "@mongodb-js/compass-database": "https://github.com/mongodb-js/compass-database",
    // "@mongodb-js/compass-databases-ddl": "https://github.com/mongodb-js/compass-databases-ddl",
    // "@mongodb-js/compass-deployment-awareness": "https://github.com/mongodb-js/compass-deployment-awareness",
    // "@mongodb-js/compass-explain-plan": "https://github.com/mongodb-js/compass-explain-plan",
    // "@mongodb-js/compass-export-to-language": "https://github.com/mongodb-js/compass-export-to-language",
    // "@mongodb-js/compass-field-store": "https://github.com/mongodb-js/compass-field-store",
    // "@mongodb-js/compass-find-in-page": "https://github.com/mongodb-js/compass-find-in-page",
    // "@mongodb-js/compass-home": "https://github.com/mongodb-js/compass-home",
    // "@mongodb-js/compass-import-export": "https://github.com/mongodb-js/compass-import-export",
    // "@mongodb-js/compass-indexes": "https://github.com/mongodb-js/compass-indexes",
    // "@mongodb-js/compass-instance": "https://github.com/mongodb-js/compass-instance",
    // "@mongodb-js/compass-metrics": "https://github.com/mongodb-js/compass-metrics",
    // "@mongodb-js/compass-plugin-info": "https://github.com/mongodb-js/compass-plugin-info",
    // "@mongodb-js/compass-query-bar": "https://github.com/mongodb-js/compass-query-bar",
    // "@mongodb-js/compass-query-history": "https://github.com/mongodb-js/compass-query-history",
    // "@mongodb-js/compass-schema": "https://github.com/mongodb-js/compass-schema",
    // "@mongodb-js/compass-schema-validation": "https://github.com/mongodb-js/compass-schema-validation",
    // "@mongodb-js/compass-server-version": "https://github.com/mongodb-js/compass-server-version",
    // "@mongodb-js/compass-serverstats": "https://github.com/mongodb-js/compass-serverstats",
    // "@mongodb-js/compass-sidebar": "https://github.com/mongodb-js/compass-sidebar",
    // "@mongodb-js/compass-ssh-tunnel-status": "https://github.com/mongodb-js/compass-ssh-tunnel-status",
    // "@mongodb-js/compass-status": "https://github.com/mongodb-js/compass-status",
    // "hadron-app": "https://github.com/mongodb-js/hadron-app",
    // "hadron-app-registry": "https://github.com/mongodb-js/hadron-app-registry",
    // "hadron-auto-update-manager": "https://github.com/mongodb-js/hadron-auto-update-manager",
    // "hadron-compile-cache": "https://github.com/mongodb-js/hadron-compile-cache",
    // "hadron-document": "https://github.com/mongodb-js/hadron-document",
    // "hadron-ipc": "https://github.com/mongodb-js/hadron-ipc",
    // "hadron-module-cache": "https://github.com/mongodb-js/hadron-module-cache",
    // "hadron-plugin-manager": "https://github.com/mongodb-js/hadron-plugin-manager",
    // "hadron-build": "https://github.com/mongodb-js/hadron-build",
    // "hadron-style-manager": "https://github.com/mongodb-js/hadron-style-manager",
    // "mongodb-collection-model": "https://github.com/mongodb-js/collection-model",
    // "mongodb-connection-model": "https://github.com/mongodb-js/connection-model",
    // "mongodb-data-service": "https://github.com/mongodb-js/data-service",
    // "mongodb-database-model": "https://github.com/mongodb-js/database-model",
    // "mongodb-explain-plan-model": "https://github.com/mongodb-js/explain-plan-model",
    // "mongodb-index-model": "https://github.com/mongodb-js/index-model",
    // "mongodb-instance-model": "https://github.com/mongodb-js/instance-model",
    // "mongodb-language-model": "https://github.com/mongodb-js/mongodb-language-model",
    // "storage-mixin": "https://github.com/mongodb-js/storage-mixin"
  },
  replaceDep: (dependency) => {
    const fixVersion = {
      'react': '^16.14.0',
      'react-dom': '^16.14.0',
      'mongodb-ns': '^2.2.0',
      'mongodb-ace-autocompleter': '^0.4.12',
      'mongodb-ace-mode': '^0.4.1',
      'mongodb-query-parser': '^2.1.2',
      'mongodb-schema': '^8.2.5',
      'mongodb-js-metrics': '^5.0.2',
      'react-bootstrap': '^0.33.1',
      'bootstrap': '^3.3.5',
      'hadron-react-components': '^4.0.5',
      'rimraf': '^3.0.2',
      '@mongodb-js/compass-shell': '^0.5.0'
    };

    if (fixVersion[dependency.name]) {
      return fixVersion[dependency.name];
    }

    if([
      /karma/,
      /mocha/,
      /istanbul/,
      /nyc/,
      /commit/,
      /eslint/,
      /storybook/,
      /sinon/,
      /chai/,
      /jsdom/,
      /enzyme/,
      /xvfb-maybe/,
      /webpack/,
      /babel/,
      /@mongodb-js\/compass-license/,
      /postcss/,
      /autoprefixer/,
      /loader/
    ].find((re) => dependency.name.match(re))) {
      return undefined;
    }

    const reportDeps = dependency.type === 'dependencies' ?
      reportDependencies : reportDevDependencies;

    const report = reportDeps.find(({name}) => name === dependency.name);
    if (!report) {
      return dependency.version;
    }

    if (dependency.version === '*') {
      return report.maxVersion;
    }

    if (!semver.validRange(dependency.version)) { // not a range: ie. from repo
      return dependency.version;
    }


    const semverVersion = semver.minVersion(dependency.version).toString();

    // uniform the version to the maximum in with the same major
    for (const reportVersion of report.versions) {
      if (semver.major(reportVersion) === semver.major(semverVersion)) {
        return `^${reportVersion}`;
      }
    }

    return dependency.version;
  }
};
