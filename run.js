const fs = require('fs-extra');
const childProcess = require('child_process');
const path = require('path');
const config = require('./config');
const TARGET_DIR = 'compass';
const CLONE_DIR = 'clone';

const templatePackageJson = require('./templates/package.json');

run();

function editPackageJson(packageJsonPath, ...modifyFns) {
  const originalPackageJson = fs.readJsonSync(packageJsonPath);
  const newPackageJson = modifyFns.reduce((json, fn) => fn(json), originalPackageJson);
  fs.writeFileSync(packageJsonPath, JSON.stringify(newPackageJson, null, 2));
}

function run() {
  initializeMonorepo();
  importPackages(config.packages);

  const lernaPackages = JSON.parse(
    childProcess.execSync('lerna ls --json', {cwd: TARGET_DIR}).toString().trim()
  );

  optimizeDependencies(lernaPackages);

  applyPackageJsonTemplate();
  adjustCompassPackageJson();

  childProcess.execSync('lerna link convert', {cwd: TARGET_DIR}).toString().trim()

  // fix path for assets:

  fs.copySync(
    path.resolve(__dirname, 'templates', 'styles', 'bootstrap.less'),
    path.resolve(__dirname, 'compass/packages/compass/src/app/styles/bootstrap.less')
  );

  fs.copySync(
    path.resolve(__dirname, 'templates', 'styles', 'fontawesome.less'),
    path.resolve(__dirname, 'compass/packages/compass/src/app/styles/fontawesome.less')
  );

  stageAllAndCommit('imported all');
}

function applyPackageJsonTemplate() {
  editPackageJson(
    path.resolve(TARGET_DIR, 'package.json'),
    (originalPackageJson) => {
      return {
        ...originalPackageJson,
        name: 'compass-monorepo',
        engines: {
          node: '>= 12.0.0'
        },
        scripts: {
          ...originalPackageJson.scripts,
          ...templatePackageJson.scripts
        },
        devDependencies: {
          ...originalPackageJson.devDependencies,
          ...templatePackageJson.devDependencies
        }
      };
    }
  );
}

function adjustCompassPackageJson() {
  editPackageJson(
    path.resolve(TARGET_DIR, 'packages', 'compass', 'package.json'),
    (originalPackageJson) => {
      const stringifiedContent = JSON.stringify({ ...originalPackageJson, private: true })
      .replace(/node_modules/g, '../../node_modules');
      return JSON.parse(stringifiedContent);
    }
  );
}

function optimizeDependencies(lernaPackages) {
  const lernaPackagesVersions = {};
  lernaPackages.forEach(p => lernaPackagesVersions[p.name] = p.version);

  for (const lernaPackage of lernaPackages) {
    editPackageJson(
      path.join(lernaPackage.location, 'package.json'),
      (originalPackageJson) => {
        const newPackageJson = {
          ...originalPackageJson,
          dependencies: {
            ...originalPackageJson.dependencies
          },
          devDependencies: {
            ...originalPackageJson.devDependencies
          }
        };

        // remove dev deps that are already in the root
        for (const dep of Object.keys(templatePackageJson.devDependencies)) {
          delete newPackageJson.devDependencies[dep];
        }

        const { dependencies, devDependencies } = newPackageJson;

        for (const [type, deps] of [
          ['dependencies', dependencies],
          ['devDependencies', devDependencies]
        ]) {
          // align all the versions for imported packages to one
          for (const dependencyName of Object.keys(deps)) {
            alignVersionsForImportedPackages(
              lernaPackagesVersions,
              dependencyName,
              deps,
              type
            );

            // do specific dep changes:
            const newVersion = config.replaceDep({
              name: dependencyName,
              version: deps[dependencyName],
              type: type
            }, newPackageJson.name);

            if (newVersion !== deps[dependencyName]) {
              logReplacement(dependencyName, deps[dependencyName], newVersion);

              if (!newVersion) {
                delete deps[dependencyName];
              } else {
                deps[dependencyName] = newVersion;
              }
            }
          }
        }

        return newPackageJson;
      }
    );

  }
}

function logReplacement(depName, oldVersion, newVersion) {
  console.log(
    '> replacing',
    depName, oldVersion,
    'with', newVersion);
}

function alignVersionsForImportedPackages(
  lernaPackagesVersions,
  dependencyName,
  deps,
  type
) {
  const importedPackageVersion = lernaPackagesVersions[dependencyName];
  if (!importedPackageVersion) {
    return;
  }

  if (type === 'devDependencies') {
    delete deps[dependencyName] // the dep should already be hoisted top-level
  }

  // const newVersion =
  //   type === 'devDependencies' ?
  //     `file:./packages/${dependencyName.split('/').reverse()[0]}` :
  //     `^${importedPackageVersion}`;

  logReplacement(dependencyName, deps[dependencyName], `^${importedPackageVersion}`);

  deps[dependencyName] = `^${importedPackageVersion}`;
}

function importPackages(packagesToImport) {
  for (const [package, repo] of Object.entries(packagesToImport)) {
    console.log('importing:', package);

    importSinglePackage(package, repo);
    adaptPackage(package, repo);
  }

  console.log('All packages imported .. running optimizations');
}

function initializeMonorepo() {
  if (!fs.existsSync(TARGET_DIR)) {
    fs.mkdirSync(TARGET_DIR);
  }

  if (!fs.existsSync(CLONE_DIR)) {
    fs.mkdirSync(CLONE_DIR);
  }

  runInDir(
    `git init`, TARGET_DIR
  );

  runInDir(
    `lerna init --independent`, TARGET_DIR
  );

  const gitignoreUrl = [
    'https://www.toptal.com/developers/gitignore/api/',
    'node',
    'osx',
    'windows',
    'intellij+all',
    'visualstudiocode',
    'linux',
    'webstorm+all'
  ].join(',');

  runInDir(
    `curl -o .gitignore ${gitignoreUrl}`,
    TARGET_DIR
  );

  fs.copySync(
    path.resolve(__dirname, 'templates', 'config'),
    path.resolve(TARGET_DIR, 'config')
  );

  fs.copySync(
    path.resolve(__dirname, 'templates', 'scripts'),
    path.resolve(TARGET_DIR, 'scripts')
  );

  stageAllAndCommit('initialized repo');
}

function stageAllAndCommit(message) {

  try {
    runInDir(
      `git add .`, TARGET_DIR
    );

    runInDir(
      `git commit -am '${message}'`, TARGET_DIR
    );
  } catch (err) {
    console.log('Commit failed, skipping');
  }
}

function importSinglePackage(packageName, repoUrl) {
  if (repoUrl === '?') {
    return;
  }

  const clonedDir = cloneOrUpdate(packageName, repoUrl);

  const packageFolder = packageName.split('/').reverse()[0];

  if (process.env.IMPORT_STRATEGY === 'lerna') {
    runInDir(`lerna import -y --flatten --preserve-commit ${clonedDir}`, TARGET_DIR);
  } else {
    runInDir(`cp -R ${clonedDir} ${TARGET_DIR}/packages/${packageFolder}`);
    runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/.git`);
  }

  runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/.github`);
  runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/node_modules`);

  if (packageName.startsWith('@mongodb-js')) { // plugins
    runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/config`);
    runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/electron`);
    runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/scripts`);
    runInDir(`rm -Rf ${TARGET_DIR}/packages/${packageFolder}/.babelrc`);
    fs.copySync(
      path.resolve(__dirname, 'templates', '.babelrc.js'),
      path.resolve(`${TARGET_DIR}/packages/${packageFolder}/.babelrc.js`)
    );

    fs.copySync(
      path.resolve(__dirname, 'templates', 'config-plugin'),
      path.resolve(`${TARGET_DIR}/packages/${packageFolder}/config`)
    );
  }

  stageAllAndCommit(`imported ${packageName}`);
}

function cloneOrUpdate(package, repo) {
  if (!fs.existsSync(`${CLONE_DIR}/${package}`)) {
    runInDir(`git clone ${repo} ${package}`, CLONE_DIR);
    runInDir(`sleep 1`);
  } else if (!config.skipPull) {
    runInDir(`git pull`, `${CLONE_DIR}/${package}`);
  }

  return path.resolve(`${CLONE_DIR}/${package}`);
}

function runInDir(command, dir) {
  childProcess.execSync(command, {stdio: 'inherit', cwd: dir})
}

function adaptPackage(package) {
  const packageTargedDir = path.resolve(TARGET_DIR, 'packages', package.split('/').reverse()[0]);

  // Fix project js config
  const projectJsPath = path.resolve(packageTargedDir, 'config', 'project.js');
  if (fs.existsSync(projectJsPath)) {
    fs.writeFileSync(projectJsPath, fs.readFileSync(projectJsPath, 'utf8')
      .replace(
        'const packageJson = require(path.join(__dirname, \'/../package.json\'))',
        'const packageJson = require(\'../../../package.json\');'
      ));
  }
}
