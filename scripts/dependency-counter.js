const semver = require('semver');

module.exports = class DependencyCounter {
  constructor() {
    this._deps = {};
  }

  add(dependencyName, versionRange, fromPackage) {
    const version =
      (versionRange === '*' ? '*' :
        semver.validRange(versionRange) ? semver.minVersion(versionRange) :
          versionRange).toString()

    this._deps[dependencyName] = (this._deps[dependencyName] || {
      name: dependencyName,
      versions: [],
      dependants: {},
      count: 0,
    });

    const dep = this._deps[dependencyName];

    dep.count++;

    if (!dep.versions.includes(version)) {
      dep.versions.push(version);
    }

    dep.dependants[version] =
      dep.dependants[version] || [];
    dep.dependants[version].push(fromPackage);

    dep.versions = dep.versions.sort(
      (v1, v2) => {
        if (!semver.valid(v1)) {
          return -1
        }

        if (!semver.valid(v2)) {
          return 1;
        }

        return semver.compare(v1, v2);
      }
    ).reverse();

    dep.maxVersion = dep.versions[0];
  }

  getCountSorted() {
    return this.getCount().sort((v1, v2) => v1.count < v2.count ? -1 : 1);
  }

  getCount() {
    return Object.values(this._deps);
  }
};
