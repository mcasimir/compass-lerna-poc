const glob = require('glob').sync;
const fs = require('fs-extra');
const _ = require('lodash');
const DependencyCounter = require('./dependency-counter');

function reportDependencies(patterns = []) {
  if (!patterns.length) {
    patterns = [
      'clone/!(@mongodb-js)/package.json',
      'clone/@mongodb-js/*/package.json'
    ]
  }

  const packageJsons = _.flatten(
      patterns.map(
        (pattern) => glob(pattern)
      )
    )
    .map((packageJsonPath) => fs.readJSONSync(packageJsonPath))

  return {
    dependencies: aggregateDependencies(packageJsons, 'dependencies'),
    devDependencies: aggregateDependencies(packageJsons, 'devDependencies'),
  };
}

module.exports = reportDependencies;

if (require.main === module) {
  console.log(JSON.stringify(reportDependencies(process.argv.slice(2)), null, 2));
}

function aggregateDependencies(packages, dependencyType) {
  const dependencies = new DependencyCounter();
  const packageNames = packages.map((package) => package.name);

  for (const packageJson of packages) {
    for (const [dependency, version] of Object.entries(packageJson[dependencyType] || {})) {
      if (!packageNames.includes(dependency)) {
        dependencies.add(dependency, version, packageJson.name);
      }
    }
  }

  return dependencies.getCountSorted().reverse();
}

