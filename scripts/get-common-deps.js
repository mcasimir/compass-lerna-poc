const semver = require('semver')

const sortKeys = (obj) => {
  return Object.entries(obj)
  .sort((a, b) => a[0] > b[0] ? 1 : -1)
  .reduce((acc, curr) => {
    const ver = semver.coerce(curr[1]);

    const range = ver ? `^${ver}` : curr[1];

    return {...acc, [curr[0]]: range}
  }, {});
}

function getCommonDeps(deps) {
  const sharedDevDeps = deps
    .filter(dep => !dep.name.startsWith('@mongodb-js'))
    .filter(dep => dep.maxVersion)
    .reduce((acc, curr) => {
    return {...acc, [curr.name]: curr.maxVersion}
  }, {});

  return sortKeys(sharedDevDeps);
}

module.exports = getCommonDeps;
