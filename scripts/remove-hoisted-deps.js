const fs = require('fs');
const path = require('path');
const topLevelDevDependencies = require('../compass/package.json').devDependencies;

const deps = Object.keys(topLevelDevDependencies);

const packagesPath = path.resolve(__dirname, '..', 'compass', 'packages');

for (const packageDir of fs.readdirSync(packagesPath)) {
  const packageJsonPath = path.resolve(packagesPath, packageDir, 'package.json');
  const packageJson = require(packageJsonPath);

  if (!packageJson.devDependencies) {
    continue;
  }

  for (const dep of deps) {
    delete packageJson.devDependencies[dep];
  }

  fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
}
