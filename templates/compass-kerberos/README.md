# `kerberos` module repackaged for MongoDB compass

Repackage `kerberos` so it works in both `node` and `electron`.

## Motivation

This package is meant to avoid manual rebuilds and conflicts in compass plugins
since some tasks like unit tests will need a `node-gyp` build of the package,
and some like `start` and ui tests will require the `electron-rebuild` build of the package.

This approach completely eliminates the issue by providing both builds and dynamically
resolving the right one at run-time.

## How it works

This package uses npm aliases (`npm install <alias>@npm:<name>`) so it can
depend twice on `kerberos` under 2 different aliases `compass-kerberos-electron`
and `compass-kerberos-node`.

When this package is installed the `compass-kerberos-electron` and `compass-kerberos-node`
are rebuilt with `electron-rebuild` and `node-gyp`, creating two versions of
`kerberos` available for both platforms.

Finally, when the package is required, it detects if is running in `node` or `electron`
and returns the appropriate version for the platform.
