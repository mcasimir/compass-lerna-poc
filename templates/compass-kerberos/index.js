const isElectron = require('is-electron');

function requireKerberos() {
  if (isElectron()) {
    return require('compass-kerberos-electron');
  }

  return require('compass-kerberos-node');
}

module.exports = requireKerberos();
