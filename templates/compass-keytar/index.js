const isElectron = require('is-electron');

function requireKeytar() {
  if (isElectron()) {
    return require('compass-keytar-electron');
  }

  return require('compass-keytar-node');
}

module.exports = requireKeytar();