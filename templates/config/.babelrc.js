const electronVersion = require('electron/package.json').version;

module.exports = {
  "presets": [
    [
      "@babel/preset-env",
      {
        "targets": {
          "electron": electronVersion
        }
      }
    ],
    "@babel/preset-react"
  ],
  "plugins": [
    "@babel/plugin-transform-runtime",
    "@babel/plugin-proposal-class-properties"
  ]
}
