const chokidar = require('chokidar')
const path = require('path')
const lerna = require('lerna')
const execa = require('execa')

let changed = new Set();
let inProgress = false;

chokidar.watch(
  'packages/{src,styles}/*.{js,less,jsx,ts,tsx}', {
    ignored: '**/node_modules/**',
    ignoreInitial: true
  }
).on('all', (event, changedPath) => {
  const packageJson = path.join(
    ...changedPath.split(path.sep).slice(0, 2),
    'package.json'
  );

  changed.add(require(`..${path.sep}${packageJson}`).name);
});

setInterval(async() => {
  if (inProgress || !changed.size) {
    return;
  }

  inProgress = true;
  const scope = Array.from(changed).join(',');
  changed = new Set();

  try {
    await execa('lerna', ['run', '--scope', scope, 'compile'], {
      stdio: 'inherit'
    });

  } finally {
    inProgress = false;
  }
}, 100);